import json, os, discord, logging, bytef, ascii85, hashlib, subprocess, binascii, math, urllib.request

# __/\\\________/\\\________________________________________________________        
#  _\/\\\_____/\\\//_________________________________________________________       
#   _\/\\\__/\\\//_______________________________________________________/\\\_      
#    _\/\\\\\\//\\\______/\\\\\\\\\_______/\\\\\__/\\\\\____/\\\____/\\\_\///__     
#     _\/\\\//_\//\\\____\////////\\\____/\\\///\\\\\///\\\_\/\\\___\/\\\__/\\\_    
#      _\/\\\____\//\\\_____/\\\\\\\\\\__\/\\\_\//\\\__\/\\\_\/\\\___\/\\\_\/\\\_   
#       _\/\\\_____\//\\\___/\\\/////\\\__\/\\\__\/\\\__\/\\\_\/\\\___\/\\\_\/\\\_  
#        _\/\\\______\//\\\_\//\\\\\\\\/\\_\/\\\__\/\\\__\/\\\_\//\\\\\\\\\__\/\\\_ 
#         _\///________\///___\////////\//__\///___\///___\///___\/////////___\///__                                                                                    
#          ____________________˜”*°•.˜”*°• Shayna •°*”˜.•°*”˜________________________
#           _________________________ ⋰C⋰h⋰a⋰r⋰l⋰e⋰s⋰ _______________________________
                                                                                                                              
#region Logger Setup
logging.basicConfig(filename = "execution.log", level=logging.INFO, format = "%(asctime)s -> %(levelname)s: %(message)s")
logger = logging.getLogger()
#endregion

logger.info("===================================== E X E C U T I O N ======================================")

config = {}

# region File Generation
if not os.path.isdir("json"):
    os.makedirs("json")
    logger.log("Created Json directory.")

if not os.path.isfile("json/.requisites"):
    with open("json/.requisites", "w+") as io:
        io.write(json.dumps({}))
        io.close()
        logger.info("Generated .requisite list")

with open("json/.requisites") as io:
    requisites = json.loads(io.read())
    io.close()

for requisite in requisites:
    if not os.path.isfile("json/"+requisite):
        with open("json/" + requisite, "w+") as io:
            io.write(json.dumps(requisites[requisite]))
            io.close()
            logger.info("Generated requisite file - " + requisite)
#endregion

def configStore(scope="*"):
    ''' 
        Function that stores the config stored in memory back onto the drive. 
        Takes scope parameter, * meaning every config file.
        Can be replaced with specific config file name to only store that file.
    '''
    if scope == "*":
        for cfgFile in config:
            with open("json/" + cfgFile, "w") as io:
                io.write(json.dumps(config[cfgFile]))
                io.close()
    else:
        if scope in config:
            with open("json/" + scope, "w") as io:
                io.write(json.dumps(config[scope]))
                io.close()
        else:
            raise Exception("Scope not in config.") 
def configLoad(scope="*"):
    '''
        Function that loads config file(s) from the disk into memory.
        Takes scope parameter, * meaning every config file.
        Can be replaced with specific config file name to only load that file.
    '''
    if scope == "*":
        for _file in os.listdir("json"):
            if _file.endswith(".json") and not _file.startswith("."):
                with open("json/" + _file, "r") as io:
                    config[_file] = json.loads(io.read())
                    io.close()
    else:
        if scope.endswith(".json") and not scope.startswith(".") and scope in os.listdir("json"):
            with open("json/"+scope, "r") as io:
                config[scope] = json.loads(io.read())
                io.close()      
        else:
            raise Exception("Invalid Scope.")
configLoad()

kamui = discord.Client()

@kamui.event
async def on_ready():
    print("Kamui initialized. Client ID - " + str(kamui.user.id))
    logger.info("Kamui initialized. Client ID - " + str(kamui.user.id))

class command_core:
    cmdGlobals = {
        "operations":{
            "*":lambda x,y: x*y,
            "/":lambda x,y: x/y,
            "+":lambda x,y: x+y,
            "-":lambda x,y: x-y,
            "^":lambda x,y: x**y,
            "%":lambda x,y: x%y,
            "_":lambda x,y: (x*y)/100,
            "~":lambda x,y: (x/y)*100
        },  
        "calc_buffers":{},
        "morse_code": { 
            '': '/', '0': '-----', '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....', '6': '-....', '7': '--...', '8': '---..', '9': '----.', 'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--', 'Z': '--..', " ":"/"
        },

        "morse_code_reverse":{
            '-----': '0', '.----': '1', '..---': '2', '...--': '3', '....-': '4', '.....': '5', '-....': '6', '--...': '7', '---..': '8', '----.': '9', '.-': 'A', '-...': 'B', '-.-.': 'C', '-..': 'D', '.': 'E', '..-.': 'F', '--.': 'G', '....': 'H', '..': 'I', '.---': 'J', '-.-': 'K', '.-..': 'L', '--': 'M', '-.': 'N', '---': 'O', '.--.': 'P', '--.-': 'Q', '.-.': 'R', '...': 'S', '-': 'T', '..-': 'U', '...-': 'V', '.--': 'W', '-..-': 'X', '-.--': 'Y','--..': 'Z', "/":" "
        },
    }
    async def commandResolution(self, message):
        ''' Resolves a message and executes the ascociated command (if any). '''
        #region Arithmatic
        if message.content.lower().startswith("mt!calc~"):
            await self.cmd_calculate(message)
        if message.content.lower().startswith("mt!percof~"):
            await self.cmd_percof(message)
        if message.content.lower().startswith("mt!percval~"):
            await self.cmd_percval(message)
        if message.content.lower().startswith("mt!square~"):
            await self.cmd_square(message)
        if message.content.lower().startswith("mt!cube~"):
            await self.cmd_cube(message)
        if message.content.lower().startswith("mt!squareroot~"):
            await self.cmd_root_square(message)
        if message.content.lower().startswith("mt!cuberoot~"):
            await self.cmd_root_cube(message)
        if message.content.lower().startswith("mt!floor~"):
            await self.cmd_floor(message)
        if message.content.lower().startswith("mt!round~"):
            await self.cmd_round(message)
        #endregion
        #region Plate Registration Commands
        if message.content.lower().startswith("pl!reg->"):
            await self.verifyPermission(message, self.cmd_registerplate)
        if message.content.lower().startswith("pl!show->"):
            await self.verifyPermission(message, self.cmd_findplate)
        if message.content.lower().startswith("pl!del->"):
            await self.verifyPermission(message, self.cmd_removeplate)
        if message.content.lower().startswith("pl!list"):
            await self.verifyPermission(message, self.cmd_platelist)
        #endregion
        #region Administration Commands
        if message.content.lower().startswith("ad!reloadcfg"):
            await self.verifyPermission(message, self.cmd_reloadconfig)
        if message.content.lower().startswith("ad!shutdown"):
            await self.verifyPermission(message, self.cmd_shutdown)
        if message.content.lower().startswith("ad!logsize"):
            await self.verifyPermission(message, self.cmd_logsize)
        if message.content.lower().startswith("ad!clearlog"):
            await self.verifyPermission(message, self.cmd_clearlog)
        #endregion
        #region Misc Commands
        if message.content.lower().startswith("!help"):
            await self.cmd_help(message)
        if message.content.lower().startswith("ad!help"):
            await self.verifyPermission(message, self.cmd_adminhelp)
        if message.content.lower().startswith("!ping"):
            await self.verifyPermission(message, self.cmd_ping)
        #endregion
        #region Cipher Commands
        if message.content.lower().startswith("ci!evig~"):
            await self.cmd_enc_vigenere(message)
        if message.content.lower().startswith("ci!dvig~"):
            await self.cmd_dec_vigenere(message)
        if message.content.lower().startswith("ci!ecc~"):
            await self.cmd_enc_caesar(message)
        if message.content.lower().startswith("ci!dcc~"):
            await self.cmd_dec_caesar(message)
        if message.content.lower().startswith("ci!rot13~"):
            await self.cmd_rot13(message)
        if message.content.lower().startswith("ci!emorse~"):
            await self.cmd_enc_morse(message)
        if message.content.lower().startswith("ci!dmorse~"):
            await self.cmd_dec_morse(message)
        if message.content.lower().startswith("ci!ebin~"):
            await self.cmd_enc_bin(message)
        if message.content.lower().startswith("ci!dbin~"):
            await self.cmd_dec_bin(message)
        if message.content.lower().startswith("ci!ehex~"):
            await self.cmd_enc_hex(message)
        if message.content.lower().startswith("ci!dhex~"):
            await self.cmd_dec_hex(message)
        if message.content.lower().startswith("ci!eb32~"):
            await self.cmd_enc_b32(message)
        if message.content.lower().startswith("ci!db32~"):
            await self.cmd_dec_b32(message)

        if message.content.lower().startswith("ci!eb64~"):
            await self.cmd_enc_b64(message)
        if message.content.lower().startswith("ci!db64~"):
            await self.cmd_dec_b64(message)

        if message.content.lower().startswith("ci!eb85~"):
            await self.cmd_enc_b85(message)
        if message.content.lower().startswith("ci!db85~"):
            await self.cmd_dec_b32(message)

        if message.content.lower().startswith("ci!ea85~"):
            await self.cmd_enc_a85(message)
        if message.content.lower().startswith("ci!da85~"):
            await self.cmd_dec_a85(message)
        
        #endregion
        #region Hashing Commands
        if message.content.lower().startswith("ci!sha~"):
            await self.cmd_sha(message)
        if message.content.lower().startswith("ci!md5~"):
            await self.cmd_md5(message)    
        if message.content.lower().startswith("ci!hmac~"):
            await self.cmd_hmac(message)
        #endregion
        #region Base Numbersys Commands
        if message.content.lower().startswith("bn!conv~"):
            await self.cmd_convertBase(message)
        #endregion
        #region File Encryption Commands
        if message.content.lower().startswith("fc!encrypt~"):
            await self.cmd_fileEncrypt(message)
        if message.content.lower().startswith("fc!decrypt~"):
            await self.cmd_fileDecrypt(message)
        if message.content.lower().startswith("fc!ehse~"):
            await self.cmd_fileEncryptAdv(message)
        if message.content.lower().startswith("fc!dhse~"):
            await self.cmd_fileDecryptAdv(message)
        #endregion

    async def verifyPermission(self, message, method):
        ''' Calls the given command method as long as the command executor has permission to execute it. '''
        if message.author.id in config["perms.json"]["admins"]:
            await method(message)
        else:
            try:
                if message.author.id in config["perms.json"][method.__name__]:
                    await method(message)
                else:
                    await kamui.send_message(message.channel, "You are not authorized to use this command.")
            except:
                await kamui.send_message(message.channel, "You are not authorized to use this command.")
            
    #region ===== Command Functions
    # --------------------------------
    #region Arithmatic Commands
    async def cmd_calculate(self, message):
        expression = "".join(message.content.split("~")[1:]).replace("~","")
        result = eval(str(expression))#not safe at all, very lazy, if you are actually going to be releasing this to other people/the general public for the love of fucking god replace this with some AST method
        await kamui.send_message(message.channel, "`({0}) = {1}`".format(str(expression),str(result)))
    async def cmd_percof(self, message):
        # mt!percof~x~y
        value = message.content.split("~")[1].replace(" ", "")
        maximum = message.content.split("~")[2].replace(" ", "")

        try: value = float(value)
        except:
            await kamui.send_message(message.channel, "\"{0}\" Isn't a number.".format(value))
            return
        
        try: maximum = float(maximum)
        except:
            await kamui.send_message(message.channel, "\"{0}\" Isn't a number.".format(maximum))
            return

        result = (value/maximum)*100
        await kamui.send_message(message.channel, "{0} is {1}% of {2}".format(str(value), str(result), str(maximum)))
    async def cmd_percval(self, message):
        #mt!percval~x~y
        percent = message.content.split("~")[1].replace(" ", "")
        maximum = message.content.split("~")[2].replace(" ", "")

        try: percent = float(percent)
        except:
            await kamui.send_message(message.channel, "\"{0}\" Isn't a number.".format(percent))
            return
        
        try: maximum = float(maximum)
        except:
            await kamui.send_message(message.channel, "\"{0}\" Isn't a number.".format(maximum))
            return

        result = (percent*maximum)/100
        await kamui.send_message(message.channel, "{0}% is {1} of {2}".format(str(percent), str(result), str(maximum))) 
    
    async def cmd_square(self,message):
        await kamui.send_message(message.channel,"{0}^2={1}".format(message.content.split("~")[1][0:len(message.content.split("~")[1])],str(pow(float(message.content.split("~")[1][0:len(message.content.split("~")[1])]),2)))) 
    async def cmd_cube(self,message):
        await kamui.send_message(message.channel,"{0}^3={1}".format(message.content.split("~")[1][0:len(message.content.split("~")[1])],str(pow(float(message.content.split("~")[1][0:len(message.content.split("~")[1])]),3))))
    async def cmd_root_square(self,message):
       await kamui.send_message(message.channel,"{0}^0.5={1}".format(message.content.split("~")[1][0:len(message.content.split("~")[1])],str(pow(float(message.content.split("~")[1][0:len(message.content.split("~")[1])]),1/2)))) 
    async def cmd_root_cube(self,message):
        await kamui.send_message(message.channel,"{0}^(1/3)={1}".format(message.content.split("~")[1][0:len(message.content.split("~")[1])],str(pow(float(message.content.split("~")[1][0:len(message.content.split("~")[1])]),1/3)))) 
    async def cmd_floor(self,message):
        await kamui.send_message(message.channel,"floor({0})={1}".format(message.content.split("~")[1],str(math.floor(float(message.content.split("~")[1])))))
    async def cmd_round(self,message):
        await kamui.send_message(message.channel,"round({0})={1}".format(message.content.split("~")[1],str(round(float(message.content.split("~")[1])))))
    #endregion
    #region Plate Registry Commands
    async def cmd_findplate(self, message):
        plate = message.content.split("->")[1].upper()
        if plate in config["plates.json"]:
            await kamui.send_message(message.channel, "Match for {0} found ~ {1}".format(plate, config["plates.json"][plate]))
            logger.info("Successful lookup of plate " + plate)
        else: 
            await kamui.send_message(message.channel, "No match found for " + plate)
            logger.info("Failed lookup of plate " + plate)
    async def cmd_registerplate(self, message):
        plate = message.content.split("->")[1].upper()
        desc = message.content.split("->")[2]
        config["plates.json"][plate] = desc
        logger.info("New plate registered, " + plate + " with description: " + desc)
        configStore(scope="plates.json")
        await kamui.send_message(message.channel, "The plate " + plate + " has been registered with the description: " + desc)
    async def cmd_removeplate(self, message):
        plate = message.content.split("->")[1].upper()
        if plate in config["plates.json"]:
            config["plates.json"].pop(plate)
            logger.info("Removed Plate - " + plate)
            configStore(scope="plates.json")
            await kamui.send_message(message.channel, "Removed plate " + plate)
        else:
            await kamui.send_message(message.channel, "Unable to find plate " + plate)
            logger.info("Failed to remove plate " + plate + " as plate wasn't found.")
    async def cmd_platelist(self, message):
        platelist = ""
        for plate in config["plates.json"]:
            platelist += plate + " - " + config["plates.json"][plate] + "\n"
        if platelist == "":
            await kamui.send_message(message.channel, "No plates have been found.")
        else:
            await kamui.send_message(message.channel, platelist)
    #endregion
    #region Administration Commands
    async def cmd_reloadconfig(self, message):
        configLoad()
        await kamui.send_message(message.channel, "Configuration has been re-loaded.")
    async def cmd_shutdown(self, message):
        await kamui.send_message(message.channel, "Shutting Down..")
        logger.info("============================= SHUTTING DOWN AS PER REQUEST =============================== " + str(message.author.id))
        raise SystemExit()
    async def cmd_logsize(self, message):
        with open("execution.log", "r") as io:
            log = io.read()
            io.close()
        logsize = len(log)
        loglines = len(log.split("\n"))
        await kamui.send_message(message.channel, "Logsize: {0}KB, {1} Lines".format(str(round(logsize/1024)), str(loglines)))
    async def cmd_clearlog(self, message):
        with open("execution.log", "w") as io:
            io.write("")
            io.close()
        await kamui.send_message(message.channel, "Log Cleared.")

    #endregion
    #region Misc Commands

    async def cmd_help(self, message):
        #_embed_description = config["_helpMessages"]["standardHelp"]
        #_embed = discord.Embed(title="Standard Help", description=_embed_description, color=discord.Color.purple())
        #await kamui.send_message(message.channel, embed=_embed)
        pass

    async def cmd_adminhelp(self, message):
        #_embed_description = configuration["_helpMessages"]["adminHelp"]
        #_embed = discord.Embed(title="Admin Help", description=_embed_description, color=discord.Color.purple())
        #await kamui.send_message(message.channel, embed=_embed)
        pass

    async def cmd_ping(self, message):
        address = message.content.split("~")[1]
        dembed = discord.Embed(title="Pinging ~ " + address, description="Pinging Address..", colour=discord.Colour.dark_grey())
        initial_message = await kamui.send_message(message.channel, embed=dembed)

        response = os.popen("ping {0} -n 1".format(address))
        formatted = "".join(response.read().split("\n")[0:3])
        if "Request timed out." not in formatted and "General failure" not in formatted and "Ping request could not find host" not in formatted:
            dembed = discord.Embed(title="Ping Result ~ " + address, description=formatted, colour=discord.Color.green())
            dembed.set_thumbnail(url="https://i.imgur.com/xvonbGZ.png")
        elif "Ping request could not find host" in formatted:
            dembed = discord.Embed(title="Ping Result ~ " + address, description=formatted, colour=discord.Color.gold())
            dembed.set_thumbnail(url="https://i.imgur.com/Ea3hkAB.png")
        
        else:
            dembed = discord.Embed(title="Ping Result ~ " + address, description=formatted, colour=discord.Color.red())
            dembed.set_thumbnail(url="https://i.imgur.com/uFVZjyy.png")

        await kamui.edit_message(initial_message, embed=dembed)
    
    #endregion
    #region Cipher Commands
    async def cmd_enc_vigenere(self, message):
        key = message.content.split("~")[1]
        msg = "".join(message.content[message.content.index("~", message.content.index("~")+1)+1:])
        encoded = bytef.vigenere(msg, key)

        dembed = discord.Embed(title="Vigenere Encode ({0})".format(key), description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg, encoded), colour=discord.Colour.green())   
        dembed.set_thumbnail(url="https://i.imgur.com/NeYvjQn.png")

        await kamui.send_message(message.channel, embed=dembed)
    async def cmd_dec_vigenere(self, message):
        key = message.content.split("~")[1]
        msg = "".join(message.content[message.content.index("~", message.content.index("~")+1)+1:])
        decoded = bytef.vigenere(msg, key, decode=True)

        dembed = discord.Embed(title="Vigenere Decode ({0})".format(key), description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg, decoded), colour=discord.Colour.green())   
        dembed.set_thumbnail(url="https://i.imgur.com/NeYvjQn.png")

        await kamui.send_message(message.channel, embed=dembed)
    async def cmd_enc_caesar(self, message):
        key = message.content.split("~")[1]
        msg = "".join(message.content[message.content.index("~", message.content.index("~")+1)+1:])
        try: int(key)
        except:
            await kamui.send_message(message.channel, "The specified key isn't a number.")
            return

        encoded = bytef.caesar(msg, int(key))

        dembed = discord.Embed(title="Caesar Encode ({0})".format(key), description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg, encoded), colour=discord.Colour.green())   
        dembed.set_thumbnail(url="https://i.imgur.com/6esShvx.png")

        await kamui.send_message(message.channel, embed=dembed)
    async def cmd_dec_caesar(self, message):
        key = message.content.split("~")[1]
        msg = "".join(message.content[message.content.index("~", message.content.index("~")+1)+1:])
        try: int(key)
        except:
            await kamui.send_message(message.channel, "The specified key isn't a number.")
            return

        decoded = bytef.caesar(msg, int(key), decode=True)

        dembed = discord.Embed(title="Caesar Decode ({0})".format(key), description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg, decoded), colour=discord.Colour.green())   
        dembed.set_thumbnail(url="https://i.imgur.com/6esShvx.png")

        await kamui.send_message(message.channel, embed=dembed) 
    async def cmd_rot13(self, message):
        msg = message.content[message.content.index("~")+1:]
        dembed = discord.Embed(title="Rot13 Application", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg, bytef.rot13(msg)), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/fdT9c6D.png")
        await kamui.send_message(message.channel, embed=dembed)
    
    async def cmd_enc_morse(self, message):
        msg = message.content[message.content.index("~")+1:]
        fullStr = ""
        for c in range(0,len(msg)):
            try:
                fullStr+=self.cmdGlobals["morse_code"][msg[c].upper()]+" "
            except: pass

        dembed = discord.Embed(title="Morse Encode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg, fullStr), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/T4fSpFS.png")
        await kamui.send_message(message.channel, embed=dembed)
    async def cmd_dec_morse(self, message):
        msg = message.content[message.content.index("~")+1:]
        fullStr = ""
        allMorses = msg.split(' ')
        for c in range(0,len(allMorses)):
            fullStr+=self.cmdGlobals["morse_code_reverse"][allMorses[c]]
       
        dembed = discord.Embed(title="Morse Decode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg, fullStr), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/T4fSpFS.png")
        await kamui.send_message(message.channel,embed=dembed)
    
    async def cmd_enc_bin(self,message):
        msg = message.content[message.content.index("~")+1:]
        msg_encoded = (' ').join(format(ord(c),'b') for c in msg)
        dembed = discord.Embed(title="Binary Encode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg, msg_encoded), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/9hiYdDz.png")

        await kamui.send_message(message.channel, embed=dembed)
    async def cmd_dec_bin(self,message):
        msg = message.content[message.content.index("~")+1:]
        newMsg=""
        splitMsg = msg.split(' ')
        for c in range(0,len(splitMsg)):
            newMsg+=chr(int(splitMsg[c],2))

        dembed = discord.Embed(title="Binary Decode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg, newMsg), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/9hiYdDz.png")

        await kamui.send_message(message.channel, embed=dembed)

    async def cmd_enc_hex(self, message):
        # ci!ehex~Message
        msg = message.content[message.content.index("~")+1:].encode()
        msg_hex = ascii85.b16encode(msg).upper().decode()
        
        pretty_printed = ""
        for i in range(len(msg_hex)):
            pretty_printed += str(msg_hex[i])
            if (i+1) % 2 == 0:
                pretty_printed += " "
        
        dembed = discord.Embed(title="Hexadecimal Encode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg.decode(), pretty_printed), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/5sV1BBl.png")
        
        await kamui.send_message(message.channel, embed=dembed)   
    async def cmd_dec_hex(self, message):
        # ci!dhex~message
        msg_hex = message.content[message.content.index("~")+1:].replace("`","")
        msg = ascii85.b16decode(msg_hex.replace(" ","")).decode()

        dembed = discord.Embed(title="Hexadecimal Decode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg_hex, msg), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/5sV1BBl.png")
        await kamui.send_message(message.channel, embed=dembed)
    
    async def cmd_enc_b32(self, message):
        msg = message.content[message.content.index("~")+1:].encode()
        encoded = ascii85.b32encode(msg).decode()
        
        dembed = discord.Embed(title="Base32 Encode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg.decode(), encoded), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/APdDWzV.png")
        
        await kamui.send_message(message.channel, embed=dembed)
    async def cmd_dec_b32(self, message):
        msg = message.content[message.content.index("~")+1:].encode()
        decoded = ascii85.b32decode(msg).decode()

        dembed = discord.Embed(title="Base32 Decode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg.decode(), decoded), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/APdDWzV.png")

        await kamui.send_message(message.channel, embed=dembed)
    async def cmd_enc_b64(self, message):
        msg = message.content[message.content.index("~")+1:].encode()
        encoded = ascii85.b64encode(msg).decode()

        dembed = discord.Embed(title="Base64 Encode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg.decode(), encoded), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/APdDWzV.png")

        await kamui.send_message(message.channel, embed=dembed)
    async def cmd_dec_b64(self, message):
        msg = message.content[message.content.index("~")+1:].encode()
        decoded = ascii85.b64decode(msg).decode()

        dembed = discord.Embed(title="Base64 Decode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg.decode(), decoded), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/APdDWzV.png")

        await kamui.send_message(message.channel, embed=dembed)
    
    async def cmd_enc_b85(self, message):
        msg = message.content[message.content.index("~")+1:].encode()
        encoded = ascii85.b85encode(msg).decode()

        dembed = discord.Embed(title="Base85 Encode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg.decode(), encoded), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/SDr2p6c.png")

        await kamui.send_message(message.channel, embed=dembed)
    async def cmd_dec_b85(self, message):
        msg = message.content[message.content.index("~")+1:].encode()
        decoded = ascii85.b85decode(msg).decode()

        dembed = discord.Embed(title="Base85 Decode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg.decode(), decoded), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/SDr2p6c.png")

        await kamui.send_message(message.channel, embed=dembed)

    async def cmd_enc_a85(self, message):
        msg = message.content[message.content.index("~")+1:].encode()
        encoded = ascii85.a85encode(msg).decode()

        dembed = discord.Embed(title="Ascii85 Encode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg.decode(), encoded), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/nfcq9EO.png")

        await kamui.send_message(message.channel, embed=dembed)
    async def cmd_dec_a85(self, message):
        msg = message.content[message.content.index("~")+1:].encode()
        decoded = ascii85.a85decode(msg).decode()

        dembed = discord.Embed(title="Ascii85 Decode", description="=== Target Message ===\n{0}\n\n=== Result ===\n{1}".format(msg.decode(), decoded), colour=discord.Colour.green())
        dembed.set_thumbnail(url="https://i.imgur.com/nfcq9EO.png")

        await kamui.send_message(message.channel, embed=dembed)
    
    #endregion
    #region Hashing Commands
    async def cmd_sha(self, message):
        shaProto = message.content.split("~")[1]
        shaMessage = "".join(message.content[message.content.index("~", message.content.index("~")+1)+1:])
        shaFunctions = {
            "1":lambda msg: bytef.SHA1(msg.encode()),
            "224":lambda msg: bytef.SHA224(msg.encode()),
            "256":lambda msg: bytef.SHA256(msg.encode()),
            "384":lambda msg: bytef.SHA384(msg.encode()),
            "512":lambda msg: bytef.SHA512(msg.encode())
        }

        if not shaProto in shaFunctions:
            await kamui.send_message(message.channel, "Unrecognized SHA protocol.")
            return
        
        result = shaFunctions[shaProto](shaMessage)
        await kamui.send_message(message.channel, "`{0}`".format(result))
    async def cmd_md5(self, message):
        msg = message.content.split("~")[1].encode()
        await kamui.send_message(message.channel, "`{0}`".format(hashlib.md5(msg).hexdigest()))
    async def cmd_hmac(self, message):
        # ci!hmac~sha1~msg~salt~iters~length
        seperatorList = []
        for i in range(len(message.content)):
            if message.content[i] == "~":
                seperatorList.append(i)

        hashAlgorithm = message.content.split("~")[1]
        #hashMessage = message.content.split("~")[5].encode()
        hashMessage = "".join(message.content[seperatorList[4]+1:]).encode()

        hashSalt = message.content.split("~")[4].encode()
        hashIters = message.content.split("~")[2]
        hashLength = message.content.split("~")[3]

        if not hashAlgorithm in config["algos.json"]["algos"]:
            await kamui.send_message(message.channel, "Hash algorithm not recognized.")
            return

        try: int(hashIters)
        except:
            await kamui.send_message(message.channel, "Iteration count isn't a number.")
            return

        try: int(hashLength)
        except:
            await kamui.send_message(message.channel, "Hash length isn't a number.")
            return

        hashResult = bytef.HMAC(hashMessage, hashSalt, int(hashIters), int(hashLength), hashAlgorithm)
        await kamui.send_message(message.channel, hashResult)
        
    #endregion
    #region Base Numbersys Commands
    async def convertBase(self, numberStr, from_base, to_base):
        converters = {
            "stoi": {
                "bin":lambda x: int(x, 2),
                "oct":lambda x: int(x, 8),
                "dec":lambda x: int(x, 10),
                "hex":lambda x: int(x, 16)
            },

            "itos": {
                "bin":lambda x: str(bin(x)),
                "oct":lambda x: str(oct(x)),
                "dec":lambda x: str(x),
                "hex":lambda x: str(hex(x))
            }
        }

        _original = converters["stoi"][from_base](numberStr)
        _converted = converters["itos"][to_base](_original)
        return _converted   
    async def cmd_convertBase(self, message):
        # bn!conv~hex|0x80~bin
        _original = message.content.split("~")[1].lower()
        _original_base = _original.split("|")[0].lower()
        _original_num = _original.split("|")[1].lower()
        _target_base = message.content.split("~")[2].lower()

        validBases = ["hex", "bin", "dec", "oct"]
        if _original_base not in validBases:
            await kamui.send_message(message.channel, "The base {0} isn't valid. Valid: {1}".format(_original_base, "bin,oct,dec,hex"))
            return

        if _target_base not in validBases:
            await kamui.send_message(message.channel, "The base {0} isn't valid. Valid: {1}".format(_target_base, "bin,oct,dec,hex"))
            return

        converted = None
        try: 
            converted = await self.convertBase(_original_num, _original_base, _target_base)
        except:
            await kamui.send_message(message.channel, "`{0}` Isn't a valid {1} number.".format(_original_num, _original_base))
            print("Error happened.")
            return

        # https://i.imgur.com/ucNdmyu.png
        #"=== Target Message ===\n{0}\n\n=== Result ===\n{1}"
        if converted != None:
            dembed = discord.Embed(
                title="Base Conversion ({0} - {1})".format(_original_base, _target_base), 
                description="=== Original Number [{0}] ==\n{1}\n\n=== Converted Number [{2}] ===\n{3}".format(
                    _original_base, 
                    _original_num, 
                    _target_base, 
                    converted
                ),
                colour=discord.Colour.green()
            )

            dembed.set_thumbnail(url="https://i.imgur.com/ucNdmyu.png")
            await kamui.send_message(message.channel, embed=dembed)
        else:
            print(converted)
            await kamui.send_message(message.channel, "An error has occured.")
    #endregion
    #region File Encryption Commands
    async def cmd_fileEncrypt(self, message):
        password = message.content.split("~")[1]
        attch = message.attachments
        if len(attch) == 0:
            await kamui.send_message(message.channel, "You didn't attach a file.")
            return

        if not "url" in attch[0]:
            await kamui.send_message(message.channel, "The attachment is malformed.")
            return

        file_url = attch[0]["url"]

        url_request = urllib.request.Request(file_url, headers={'User-Agent' : "Magic Browser"})
        url_reader = urllib.request.urlopen(url_request)

        file_data = url_reader.read()
        initial_message = await kamui.send_message(message.channel, "File recieved. Encrypting..")

        _HSE = bytef.HSE()
        _HSE.initialize(password.encode(), bytef.MODE_A85)

        file_data_size = len(file_data) / 1024

        file_data_e = _HSE.encrypt(file_data)
        file_data_e_size = len(file_data_e) / 1024

        
        if file_data_e_size > 8192:
            await kamui.edit_message(initial_message, "Encrypted file is too big to send.")
            return

        temp_file = "temp/" + bytef.generateKey(32) + ".encrypted_tempfile"

        with open(temp_file, "wb+") as io:
            io.write(file_data_e)
            io.close()

        embedDsc = "Filename: {0}\nUnencrypted Size: {1}KB\nEncrypted Size: {2}KB".format(attch[0]["filename"], str(round(file_data_size)), str(round(file_data_e_size)))
        dembed = discord.Embed(title="File Encryption Result", description=embedDsc, colour=discord.Colour.green())

        await kamui.edit_message(initial_message, "", embed=dembed)
        await kamui.send_file(message.channel, fp=temp_file, filename=attch[0]["filename"]+".hse")
        os.remove(temp_file)
    async def cmd_fileDecrypt(self, message):
        password = message.content.split("~")[1]
        attch = message.attachments
        if len(attch) == 0:
            await kamui.send_message(message.channel, "You didn't attach a file.")
            return

        if not "url" in attch[0]:
            await kamui.send_message(message.channel, "The attachment is malformed.")
            return

        file_url = attch[0]["url"]

        url_request = urllib.request.Request(file_url, headers={'User-Agent' : "Magic Browser"})
        url_reader = urllib.request.urlopen(url_request)

        file_data = url_reader.read()
        initial_message = await kamui.send_message(message.channel, "File recieved. Decrypting..")

        _HSE = bytef.HSE()
        _HSE.initialize(password.encode(), bytef.MODE_A85)

        file_data_size = len(file_data) / 1024

        file_data_d = _HSE.decrypt(file_data)
        file_data_d_size = len(file_data_d) / 1024

        
        if file_data_d_size > 8192:
            await kamui.edit_message(initial_message, "Encrypted file is too big to send.")
            return

        temp_file = "temp/" + bytef.generateKey(32) + ".decrypted_tempfile"

        with open(temp_file, "wb+") as io:
            io.write(file_data_d)
            io.close()

        embedDsc = "Filename: {0}\nEncrypted Size: {1}KB\nDecrypted Size: {2}KB".format(attch[0]["filename"], str(round(file_data_size)), str(round(file_data_d_size)))
        dembed = discord.Embed(title="File Decryption Result", description=embedDsc, colour=discord.Colour.green())

        await kamui.edit_message(initial_message, "", embed=dembed)
        await kamui.send_file(message.channel, fp=temp_file, filename=attch[0]["filename"]+".decrypted")
        os.remove(temp_file)
    async def cmd_fileEncryptAdv(self, message):
        # fc!ehse~a85~lzma~password
        password = message.content.split("~")[3]
        mode = message.content.split("~")[1].lower()
        compr = message.content.split("~")[2].lower()

        mode_resolver = {
            "a85":bytef.MODE_A85,
            "b85":bytef.MODE_B85,
            "b64":bytef.MODE_B64,
            "b32":bytef.MODE_B32,
            "hex":bytef.MODE_HEX,
            "bse":bytef.MODE_BSE
        }

        if mode not in mode_resolver:
            await kamui.send_message(message.channel, "Invalid mode specified.")
            return

        if compr not in ["lzma", "zlib"]:
            await kamui.send_message(message.channel, "Invalid compression specified.")
            return

        mode = mode_resolver[mode]

        attch = message.attachments
        if len(attch) == 0:
            await kamui.send_message(message.channel, "You didn't attach a file.")
            return

        if not "url" in attch[0]:
            await kamui.send_message(message.channel, "The attachment is malformed.")
            return

        file_url = attch[0]["url"]

        url_request = urllib.request.Request(file_url, headers={'User-Agent' : "Magic Browser"})
        url_reader = urllib.request.urlopen(url_request)

        file_data = url_reader.read()
        initial_message = await kamui.send_message(message.channel, "File recieved. Encrypting..")

        _HSE = bytef.HSE()
        _HSE.initialize(password.encode(), mode)

        file_data_size = len(file_data) / 1024

        file_data_e = _HSE.encrypt(file_data, compression=compr)
        file_data_e_size = len(file_data_e) / 1024

        
        if file_data_e_size > 8192:
            await kamui.edit_message(initial_message, "Encrypted file is too big to send.")
            return

        temp_file = "temp/" + bytef.generateKey(32) + ".encrypted_tempfile"

        with open(temp_file, "wb+") as io:
            io.write(file_data_e)
            io.close()

        embedDsc = "Filename: {0}\nUnencrypted Size: {1}KB\nEncrypted Size: {2}KB".format(attch[0]["filename"], str(round(file_data_size)), str(round(file_data_e_size)))
        dembed = discord.Embed(title="File Encryption Result", description=embedDsc, colour=discord.Colour.green())

        await kamui.edit_message(initial_message, "", embed=dembed)
        await kamui.send_file(message.channel, fp=temp_file, filename=attch[0]["filename"]+".hse")
        os.remove(temp_file)
    async def cmd_fileDecryptAdv(self, message):
        password = message.content.split("~")[3]
        mode = message.content.split("~")[1].lower()
        compr = message.content.split("~")[2].lower()

        mode_resolver = {
            "a85":bytef.MODE_A85,
            "b85":bytef.MODE_B85,
            "b64":bytef.MODE_B64,
            "b32":bytef.MODE_B32,
            "hex":bytef.MODE_HEX,
            "bse":bytef.MODE_BSE
        }

        if mode not in mode_resolver:
            await kamui.send_message(message.channel, "Invalid mode specified.")
            return

        if compr not in ["lzma", "zlib"]:
            await kamui.send_message(message.channel, "Invalid compression specified.")
            return

        mode = mode_resolver[mode]

        attch = message.attachments
        if len(attch) == 0:
            await kamui.send_message(message.channel, "You didn't attach a file.")
            return

        if not "url" in attch[0]:
            await kamui.send_message(message.channel, "The attachment is malformed.")
            return

        file_url = attch[0]["url"]

        url_request = urllib.request.Request(file_url, headers={'User-Agent' : "Magic Browser"})
        url_reader = urllib.request.urlopen(url_request)

        file_data = url_reader.read()
        initial_message = await kamui.send_message(message.channel, "File recieved. Decrypting..")

        _HSE = bytef.HSE()
        _HSE.initialize(password.encode(), mode)

        file_data_size = len(file_data) / 1024

        file_data_d = _HSE.decrypt(file_data, compression=compr)
        file_data_d_size = len(file_data_d) / 1024

        
        if file_data_d_size > 8192:
            await kamui.edit_message(initial_message, "Encrypted file is too big to send.")
            return

        temp_file = "temp/" + bytef.generateKey(32) + ".decrypted_tempfile"

        with open(temp_file, "wb+") as io:
            io.write(file_data_d)
            io.close()

        embedDsc = "Filename: {0}\nEncrypted Size: {1}KB\nDecrypted Size: {2}KB".format(attch[0]["filename"], str(round(file_data_size)), str(round(file_data_d_size)))
        dembed = discord.Embed(title="File Decryption Result", description=embedDsc, colour=discord.Colour.green())

        await kamui.edit_message(initial_message, "", embed=dembed)
        await kamui.send_file(message.channel, fp=temp_file, filename=attch[0]["filename"]+".decrypted")
        os.remove(temp_file)
    #endregion
    # ---------------------------------
    #endregion
    
commandCore = command_core()

@kamui.event
async def on_message(message):
    await commandCore.commandResolution(message)

kamui.run(config["bot.json"]["apikey_discord"])
