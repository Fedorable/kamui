class Converter:
    '''Class responsible for base number system conversions.'''
    baseOrders = ["Hexadecimal","Decimal","Octal","Binary"]
    def DenToHex(self,num):
        return hex(int(num))
    def DenToBin(self,num):
        return "{0:b}".format(int(num))
    def DenToOct(self,num):
        return oct(int(num))
    def BinToDen(self,num):
        return int(str(num),2)
    def BinToHex(self,num):
        return hex(self.BinToDen(num))
    def BinToOct(self,num):
        return oct(self.BinToDen(num))
    def HexToDen(self,num):
        return int(str(num),16)
    def HexToBin(self,num):
        return self.DenToBin(self.HexToDen(num))
    def HexToOct(self,num):
        return self.DenToOct(self.HexToDen(num))
    def OctToDen(self,num):
        return int(str(num),8)
    def OctToBin(self,num):
        return self.DenToBin(self.OctToDen(num))
    def OctoToHex(self,num):
        return self.DenToHex(self.OctToDen(num))
    
    #Lists return highest base to lowest base
    def DenConversion(self,num):#Works
        return [self.DenToHex(num),self.DenToOct(num),self.DenToBin(num)]
    def BinConversion(self,num):#Works
        return [self.BinToHex(num),self.BinToDen(num),self.BinToOct(num)]
    def HexConversion(self,num):#
        return [self.HexToDen(num),self.HexToOct(num),self.HexToBin(num)]
    def OctConversion(self,num):#Works
        return [self.OctoToHex(num),self.OctToDen(num),self.OctToBin(num)]

    def OutputResult(self,base,result):
        newmsg = ""
        plusIndex=False
        for c in range(0,len(result)):
            if self.baseOrders[c].startswith(base):
                plusIndex=True
            if plusIndex==True:
                newmsg+=self.baseOrders[c+1]+":"+str(result[c])+"\n"
            else:
                newmsg+=self.baseOrders[c]+":"+str(result[c])+"\n"
        return newmsg

    def DenConversionStr(self,num):
        return self.OutputResult("Decimal",self.DenConversion(num))
    def BinConversionStr(self,num):
        return self.OutputResult("Binary",self.BinConversion(num))
    def HexConversionStr(self,num):
        return self.OutputResult("Hexadecimal",self.HexConversion(num))
    def OctConversionStr(self,num):
        return self.OutputResult("Oct",self.OctConversion(num))