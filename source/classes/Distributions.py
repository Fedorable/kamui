import math
class Normal:
    mean=0
    variance=0
    def __init__(self,mean,variance):
        self.mean=mean
        self.variance=variance
    def CDF(self,x):
        return 0.5*(1+math.erf((x-self.mean)/(math.sqrt(self.variance)*math.sqrt(2))))
    def PDF(self,x):
        return (1/math.sqrt(2*math.pi*self.variance))*(math.exp(-(pow(x-self.mean,2)/(2*self.variance))))
class Binomial:
    trials=0
    probability=0
    def __init__(self,trials,probability):
        self.trials=trials
        self.probability=probability
    def PDF(self,x):
        return (math.factorial(self.trials)/(math.factorial(x)*math.factorial(self.trials-x)))*pow(self.probability,x)*pow((1-self.probability),(self.trials-x))
    #def CDF(self):#Large number of trials could lead to bad stuff tbh

class Poisson:
    average_rate=0
    def __init__(self,average_rate):
        self.average_rate=average_rate
    def PDF(self,x):
        return math.exp(-self.average_rate)*(pow(self.average_rate,x)/math.factorial(x))
    #def CDF(self,x):