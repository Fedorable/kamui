import json
from urllib.request import urlopen

currencies={"$":1,"£":2,"€":3}

base_market_price_url = "https://steamcommunity.com/market/priceoverview/?appid={0}&currency={1}&market_hash_name={2}"
base_store_price_url = "https://store.steampowered.com/api/appdetails?appids={0}"

def get_json(url):
    response=urlopen(url)
    data = response.read().decode("utf-8")
    return json.loads(data)

#Returns result in JSON
def GetMarketDetails(url,currency):#URL in this case is going to be the link to the steam market page itself. Must be in the following format: https://steamcommunity.com/market/listings/730/StatTrak%E2%84%A2%20M4A1-S%20%7C%20Hyper%20Beast%20(Factory%20New)
    splitStr=url.split("/")
    appID = splitStr[5]
    hash_name = splitStr[6]
    return get_json(base_market_price_url.format(appID,currencies[currency],hash_name))

#Returns result in JSON
def GetStoreDetails(url):
    splitStr = url.split("/")
    appID=splitStr[4]
    return get_json(base_store_price_url.format(appID))[appID]["data"]
