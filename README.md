# Kamui
Kamui is a multipurpose bot designed to be a swiss army knife. It's still in development and will mostly be a closed circle bot not intended for public use, although public use isn't prohibited (clearly open source), it's discouraged as the code is designed with a closed circle philosophy, and isn't intended for public use, so hosting it publically may pose security risks to the hoster.

It is encouraged however to host this bot and use it as a personal direct message tool-kit, as it was designed for. 

For instructions and a list of commands, please [see the wiki](https://bitbucket.org/Fedorable/kamui/wiki/Home)

\- Shayna